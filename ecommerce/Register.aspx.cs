﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace ecommerce
{
    public partial class Register : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Alert.Text = "";
            Alert.Attributes["style"] = "display: none;";
        }

        private string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["MyConnection"].ConnectionString;
        private SqlConnection conn;
        private SqlCommand cmd;
        private SqlDataReader rdr;

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (PasswordInput.Text == PasswordConfirmationInput.Text)
            {
                conn = new SqlConnection(connstr);

                cmd = new SqlCommand("select * from Users where UserName=@UserName or Email=@Email", conn);
                cmd.Parameters.AddWithValue("@Username", UserNameInput.Text);
                cmd.Parameters.AddWithValue("@Email", EmailInput.Text);

                conn.Open();
                rdr = cmd.ExecuteReader();

                if (rdr.HasRows)
                {
                    Alert.Text = "UserName or Email already registered";
                    Alert.CssClass = "alert alert-danger";
                }
                else
                {
                    string crypPassword = System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(PasswordInput.Text, "SHA1");

                    conn = new SqlConnection(connstr);
                    cmd = new SqlCommand("Insert into Users (UserName, FirstName, LastName, Email, Password, Address, Status, CreateDate) values (@UserName, @FirstName, @LastName, @Email, @Password, @Address, @Status, @CreateDate)", conn);
                    cmd.Parameters.AddWithValue("@UserName", UserNameInput.Text);
                    cmd.Parameters.AddWithValue("@FirstName", FirstNameInput.Text);
                    cmd.Parameters.AddWithValue("@LastName", LastNameInput.Text);
                    cmd.Parameters.AddWithValue("@Email", EmailInput.Text);
                    cmd.Parameters.AddWithValue("@Password", crypPassword);
                    cmd.Parameters.AddWithValue("@Address", AddressInput.Text);
                    cmd.Parameters.AddWithValue("@Status", true);
                    cmd.Parameters.AddWithValue("@CreateDate", DateTime.Now);

                    conn.Open();
                    if (cmd.ExecuteNonQuery() == 1)
                    {
                        UserNameInput.Text = "";
                        FirstNameInput.Text = "";
                        LastNameInput.Text = "";
                        EmailInput.Text = "";
                        AddressInput.Text = "";
                        PasswordInput.Text = "";
                        PasswordConfirmationInput.Text = "";
                        Alert.Text = "Registered successfully!";
                        Alert.CssClass = "alert alert-success";
                        //Response.Redirect("/Home.aspx");
                    }
                    conn.Close();
                }

                conn.Close();
            }
            else
            {
                Alert.Text = "Your Password and the Confirmation does not match";
                Alert.CssClass = "alert alert-danger";
            }

            Alert.Attributes["style"] = "display: inline;";
        }
    }
}