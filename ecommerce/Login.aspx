﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="ecommerce.Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="form-group">
        <asp:Label ID="Alert" runat="server" Text="" CssClass="alert alert-success"></asp:Label>
    </div>

    <div class="form-group">
        <label for="UserNameInput">
            <asp:Label ID="UserNameLabel" runat="server" Text="User name"></asp:Label>
        </label>
        <asp:TextBox ID="UserNameInput" runat="server" CssClass="form-control"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="UserNameInput" ErrorMessage="User name is required to login" Style="color: #FF3300"></asp:RequiredFieldValidator>
    </div>

    <div class="form-group">
        <label for="PasswordInput">
            <asp:Label ID="PasswordLabel" runat="server" Text="Password"></asp:Label>
        </label>
        <asp:TextBox ID="PasswordInput" runat="server" CssClass="form-control" TextMode="Password"></asp:TextBox>
        <asp:RequiredFieldValidator ID="PasswordInputValidator" runat="server" ControlToValidate="PasswordInput" ErrorMessage="Password is required to login" Style="color: #FF3300"></asp:RequiredFieldValidator>
    </div>

    <asp:Button ID="Button1" runat="server" Text="Login" CssClass="btn btn-primary" OnClick="Button1_Click" />
</asp:Content>