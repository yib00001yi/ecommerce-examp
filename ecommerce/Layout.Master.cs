﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ecommerce
{
    public partial class Layout : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string UserName = (string)Session["UserName"];

            if (UserName == null)
            {
                RegisterLink.Attributes["style"] = "display: inline;";
                LoginLink.Attributes["style"] = "display: inline;";
                LogoutLink.Attributes["style"] = "display: none;";
                UpdateLink.Attributes["style"] = "display: none;";
            }
            else
            {
                LoginLink.Text = UserName;
                RegisterLink.Attributes["style"] = "display: none";
                LoginLink.Attributes["style"] = "display: none";
                LogoutLink.Attributes["style"] = "display: inline";
                UpdateLink.Attributes["style"] = "display: inline;";
            }
        }
    }
}