﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace ecommerce
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Alert.Text = "";
            Alert.Attributes["style"] = "display: none;";
        }

        private string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["MyConnection"].ConnectionString;
        private SqlConnection conn;
        private SqlCommand cmd;
        private SqlDataReader rdr;
        protected void Button1_Click(object sender, EventArgs e)
        {
            Alert.Text = "Logged in";

            conn = new SqlConnection(connstr);
            string crypPassword = System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(PasswordInput.Text, "SHA1");

            cmd = new SqlCommand("select * from Users where UserName=@UserName and Password=@Password", conn);
            cmd.Parameters.AddWithValue("@Username", UserNameInput.Text);
            cmd.Parameters.AddWithValue("@Password", crypPassword);

            conn.Open();
            rdr = cmd.ExecuteReader();
            if (rdr.HasRows)
            {
                Session["UserName"] = UserNameInput.Text;
                Response.Redirect("/Home.aspx");
            }
            else
            {
                Alert.Text = "User not found or invalid password";
                Alert.CssClass = "alert alert-danger";
                Alert.Attributes["style"] = "display: inline;";
            }
            conn.Close();
        }
    }
}