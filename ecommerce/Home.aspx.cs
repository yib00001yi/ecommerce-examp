﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace ecommerce
{
    public partial class Home : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["MyConnection"].ConnectionString;
        private SqlConnection conn;
        private SqlCommand cmd;
        private SqlDataReader rdr;

        protected void Button1_Click(object sender, EventArgs e)
        {

            conn = new SqlConnection(connstr);
            cmd = new SqlCommand("select * from Products where (ProductName like @ProductName)", conn);
            cmd.Parameters.AddWithValue("@ProductName", "%" + ProductInput.Text + "%");

            System.Data.DataTable dt = new System.Data.DataTable();

            conn.Open();
            rdr = cmd.ExecuteReader();

            if (rdr.HasRows)
            {
                dt.Load(rdr);
                GridView1.DataSource = dt;
                GridView1.DataBind();
            }
            else
            {
                dt.Load(rdr);
                GridView1.DataSource = dt;
                GridView1.EmptyDataText = "No product found";
                GridView1.DataBind();
            }
            conn.Close();
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
    }
}