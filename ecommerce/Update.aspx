﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout.Master" AutoEventWireup="true" CodeBehind="Update.aspx.cs" Inherits="ecommerce.Update" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="UserIdLabel" runat="server" Text=""></asp:Label>
    <div class="form-group">
        <asp:Label ID="Alert" runat="server" Text="" CssClass="alert alert-success"></asp:Label>
    </div>

    <div class="form-group">
        <label for="UserNameInput">
            <asp:Label ID="UserNameLabel" runat="server" Text="User name"></asp:Label>
        </label>
        <asp:TextBox ID="UserNameInput" runat="server" CssClass="form-control"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="UserNameInput" ErrorMessage="User name is required" Style="color: #FF3300"></asp:RequiredFieldValidator>
    </div>

    <div class="form-group">
        <label for="FirstNameInput">
            <asp:Label ID="FirstNameLabel" runat="server" Text="First name"></asp:Label>
        </label>
        <asp:TextBox ID="FirstNameInput" runat="server" CssClass="form-control"></asp:TextBox>
        <asp:RequiredFieldValidator ID="FirstNameInputValidator" runat="server" ControlToValidate="FirstNameInput" ErrorMessage="First name is required" Style="color: #FF3300"></asp:RequiredFieldValidator>
    </div>

    <div class="form-group">
        <label for="LastNameInput">
            <asp:Label ID="LastNameLabel" runat="server" Text="Last name"></asp:Label>
        </label>
        <asp:TextBox ID="LastNameInput" runat="server" CssClass="form-control"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="LastNameInput" ErrorMessage="Last name is required" Style="color: #FF3300"></asp:RequiredFieldValidator>
    </div>

    <div class="form-group">
        <label for="EmailInput">
            <asp:Label ID="EmailLabel" runat="server" Text="Email"></asp:Label>
        </label>
        <asp:TextBox ID="EmailInput" runat="server" CssClass="form-control"></asp:TextBox>
        <asp:RequiredFieldValidator ID="EmailInputValidator" runat="server" ControlToValidate="EmailInput" ErrorMessage="Email is required" Style="color: #FF3300"></asp:RequiredFieldValidator>
    </div>

    <div class="form-group">
        <label for="AddressInput">
            <asp:Label ID="Address" runat="server" Text="Address"></asp:Label>
        </label>
        <asp:TextBox ID="AddressInput" runat="server" CssClass="form-control"></asp:TextBox>
        <asp:RequiredFieldValidator ID="AddressInputValidator" runat="server" ControlToValidate="AddressInput" ErrorMessage="Address is required" Style="color: #FF3300"></asp:RequiredFieldValidator>

    </div>

    <div class="form-group">
        <label for="PasswordInput">
            <asp:Label ID="PasswordLabel" runat="server" Text="Password"></asp:Label>
        </label>
        <asp:TextBox ID="PasswordInput" runat="server" CssClass="form-control" TextMode="Password"></asp:TextBox>
        <asp:RequiredFieldValidator ID="PasswordInputValidator" runat="server" ControlToValidate="PasswordInput" ErrorMessage="Password is required" Style="color: #FF3300"></asp:RequiredFieldValidator>
    </div>

    <div class="form-group">
        <label for="PasswordConfirmationInput">
            <asp:Label ID="PasswordConfirmation" runat="server" Text="Confirm your password"></asp:Label>
        </label>
        <asp:TextBox ID="PasswordConfirmationInput" runat="server" CssClass="form-control" TextMode="Password"></asp:TextBox>
        <asp:RequiredFieldValidator ID="PasswordConfirmationInputValidator" runat="server" ControlToValidate="PasswordConfirmationInput" ErrorMessage="Password Confirmation is required" Style="color: #FF3300"></asp:RequiredFieldValidator>
        <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="CompareValidator" Style="color: #FF3300" ControlToCompare="PasswordInput" ControlToValidate="PasswordConfirmationInput">Password Confirmation should match your Password</asp:CompareValidator>
    </div>

    <div class="form-group">
        <label for="CurrentPasswordInput">
            <asp:Label ID="CurrentPasswordLabel" runat="server" Text="Current Password"></asp:Label>
        </label>
        <asp:TextBox ID="CurrentPasswordInput" runat="server" CssClass="form-control" TextMode="Password"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="CurrentPasswordInput" ErrorMessage="Your Current Password is required to confirm your changes" Style="color: #FF3300"></asp:RequiredFieldValidator>
    </div>

    <asp:Button ID="Button1" runat="server" Text="Update" CssClass="btn btn-primary" OnClick="Button1_Click" />
</asp:Content>