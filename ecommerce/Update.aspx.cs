﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;


namespace ecommerce
{
    public partial class Update : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Alert.Text = "";
            Alert.Attributes["style"] = "display: none;";
            UserIdLabel.Attributes["style"] = "display: none;";

            string UserName = (string)Session["UserName"];

            if (UserName == null)
            {
                Response.Redirect("/Home.aspx");
            }
            else
            {
                conn = new SqlConnection(connstr);

                cmd = new SqlCommand("select * from Users where UserName=@UserName", conn);
                cmd.Parameters.AddWithValue("@Username", UserName);

                conn.Open();
                rdr = cmd.ExecuteReader();

                if (rdr.HasRows)
                {
                    while (rdr.Read())
                    {
                        UserIdLabel.Text = rdr["UserId"].ToString();
                        UserNameInput.Text = rdr["UserName"].ToString();
                        FirstNameInput.Text = rdr["FirstName"].ToString();
                        LastNameInput.Text = rdr["LastName"].ToString();
                        EmailInput.Text = rdr["Email"].ToString();
                        AddressInput.Text = rdr["Address"].ToString();
                    }
                }
                conn.Close();
            }
        }

        private string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["MyConnection"].ConnectionString;
        private SqlConnection conn;
        private SqlCommand cmd;
        private SqlDataReader rdr;

        protected void Button1_Click(object sender, EventArgs e)
        {
            conn = new SqlConnection(connstr);
            string crypPassword = System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(PasswordInput.Text, "SHA1");

            cmd = new SqlCommand("select * from Users where UserName=@UserName and Password=@Password", conn);
            cmd.Parameters.AddWithValue("@Username", UserNameInput.Text);
            cmd.Parameters.AddWithValue("@Password", crypPassword);

            conn.Open();
            rdr = cmd.ExecuteReader();
            if (rdr.HasRows)
            {
                conn = new SqlConnection(connstr);

                cmd = new SqlCommand("update Users set UserName=@UserName, Password=@Password, Email=@Email, FirstName=@FirstName, LastName=@LastName where UserId=@UserId", conn);
                cmd.Parameters.AddWithValue("@UserId", UserIdLabel.Text);
                cmd.Parameters.AddWithValue("@UserName", UserNameInput.Text);
                cmd.Parameters.AddWithValue("@FirstName", FirstNameInput.Text);
                cmd.Parameters.AddWithValue("@LastName", LastNameInput.Text);
                cmd.Parameters.AddWithValue("@Email", EmailInput.Text);
                cmd.Parameters.AddWithValue("@Password", crypPassword);
                cmd.Parameters.AddWithValue("@Address", AddressInput.Text);
                conn.Open();

                if (cmd.ExecuteNonQuery() == 1)
                {
                    Alert.Text = "Profile updated successful";
                    Alert.CssClass = "alert alert-danger";
                    Alert.Attributes["style"] = "display: inline;";
                }
                conn.Close();

            }
            else
            {
                Alert.Text = "Your Current Password does not match";
                Alert.CssClass = "alert alert-danger";
                Alert.Attributes["style"] = "display: inline;";
            }
        }
    }
}