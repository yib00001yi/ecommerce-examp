﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="ecommerce.Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="form-group">
        <label for="ProductInput">
            <asp:Label ID="ProductLabel" runat="server" Text="Product name"></asp:Label>
        </label>
        <asp:TextBox ID="ProductInput" runat="server" CssClass="form-control"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ProductInput" ErrorMessage="Product name is required to search" Style="color: #FF3300"></asp:RequiredFieldValidator>
    </div>

    <asp:Button ID="Button1" runat="server" Text="Search" CssClass="btn btn-info" OnClick="Button1_Click" />
    <asp:GridView ID="GridView1" runat="server" CssClass="table" OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
    </asp:GridView>
</asp:Content>
